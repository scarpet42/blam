# BLAM L'Affreux Mulot

## Version courte :
C'est l'histoire d'un affreux mulot qui a voulu se brancher sur mon ordinateur.
Et BLAM L'Affreux Mulot...

There is a lot of more French guys who speak English than English guys who speak French.
And it looks like the guys who don't have English or French for first language are more likely to understand English than French.
I agree, it's weird, but it seems to be a fact.
Contrary to my other projects, this one can be of interest for non-French users.
As I'm a French guy, I stuck with the French version for the joke.
But the rest of this project will be written in English.

## English explanation :

The first sentence could be translated like that :
It's the story of an awful rat which tried to plug itself on my computer.
The second sentence loose all of the pronunciation but could be translated like that :
And BTAR The Awful Rat...

There is three references with the French version :

### A joke based on the sound

I don't know if there is the same popular joke in English.
The initial French joke (Paf le chien) could be translated like :
You know the story of Bang the dog ?
It's a dog who crossed the road.
Came a car.
And Bang the dog...
The English translation of my story lost the sound.
If you get a better translation I take it...
... as long as it keeps the two other references.

### French President mistake :

Some years ago the French president made a mistake which amused the computer community.
He used the word "mulot" (which is not exactly the translation of rat) instead of "souris" (which is the translation of mouse and the right term in computer).

### A recursive acronym as in a lot of GNU projects.

It's a real acronym, which mean it's pronounced like a word.
It's not enough to be an abbreviation to be an acronyme, even if too many people do the mistake (in French and in English the mistake is the same).
And it's recursive because the first word of the sound/acronym is the sound/acronym itself.

## Purpose of this project  :

As you should have understood with the joke, I don't like mouses on my computer.
So, I use neither Microsoft©'s products nor Apple©'s one at home.
I use others which work together.
So here I will put my configurations' files to be able to use them when needed.
If you want them to inspire you or to use it directly, you can.
It's for me, so I can't assure everything will work on your computer as it.

I'll try to go a little bit further than only sharing my files.
It's boring to copy every file from the repo to the home directory.
So, I'll try to write a script able to automatically copy the files where needed.

There is still another thing to take care of.
For the files to be able to be used, some packages need to be installed.
As I'm using an Archlinux distribution, I'll try to install everything needed.
So, my purpose will me to automatically deploy my computer's configuration on a minimal Archlinux installation.
With a script able to deploy the files on another distro once installed.
And the configuration files available for anyone who wants only some file.
The purpose is for me, I just share in case anyone is interested.

## License

The license is the WTFPL : [WTFPL](http://www.wtfpl.net/)
It's at the same time the most free and the most easy to understand of all the licenses.
The freedom is very important and my time is precious : I don't have time to read, understand and compare the MIT, BSD, [L]GPL[2|3] and all the other licenses.
