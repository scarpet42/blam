"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"  Auteur : Stéphane CARPENTIER
"  Fichier : tex.vim
"    Modif : mar. 02 mars 2021 22:31
"
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" To see the mistake I could make in writing French or English.
:setlocal spell spelllang=en,fr
:syntax spell toplevel

" The key mappings to use the keyboard------------------------ {{{

"To be able to select words
:set iskeyword+=:

" To be able to use the ALT key
:set winaltkeys=no

" To be able to use my é in LaTeX
:inoremap <buffer> <leader>it <Plug>Tex_InsertItemOnThisLine
" To be able to use â in LaTeX
:inoremap <Alt-B> <Plug>Tex_MathBF

" I'm writing mostly in French, so I want French double quotes
:let g:Tex_SmartQuoteOpen  =   "«~"
:let g:Tex_SmartQuoteClose =   "~»"

" For the spell plugin to take care of the LaTeX commands
:let g:tex_verbspell=1

" To map the keys automatically, I let it there in case I need it
" It's for French typography
":inoremap <Char27> <C-V>u00a0!
":inoremap <Char27> ~!

" End of the key mapping --------------------- }}}

" Abbreviations ---------------------------------- {{{

" abbreviations to help writing the personal LaTeX commands
:call IMAP('ANN', "\\Annee{<+annee+>}", 'tex')
:call IMAP('BAS', "\\begin{listbash}\<CR><++>\<CR>\\end{listbash}", 'tex')
:call IMAP('CIT', "\\citation{<+auteur+>}{<+citation+>}{<+traduction+>}", 'tex')
:call IMAP('COC', "\\liglat{<+code en ligne+>}", 'tex')
:call IMAP('COL', "\\begin{lstlisting}\<CR><+code sur plusieurs lignes+>\<CR>\\end{lstlisting}", 'tex')
:call IMAP('CON', "\\Concept{<+Nom du concept+>}", 'tex')
:call IMAP('COU', "\\begin{couleur}{<+c pour couleur g pour gris +>}\<CR><+texte+>\<CR>\\end{couleur}", 'tex')
:call IMAP('DEF', "\\Definition{<+type c ou o+>}{<+Nom+>}{<+Définition du nom+>}", 'tex')
:call IMAP('EMP', "\\Emphase{<+texte+>}", 'tex')
:call IMAP('FIC', "\\begin{monfichier}{<+dockerfile|yaml+>}{<+NomFic+>}\<CR><++>\<CR>\\end{monfichier}", 'tex')
:call IMAP('FRA', "\\propre[n]{x}{France}", 'tex')
:call IMAP('IMA', "\\Image{<+image+>}", 'tex')
:call IMAP('IND', "\\Index{<++>}", 'tex')
:call IMAP('ITE', "\\item ", 'tex')
:call IMAP('LAT', "\\LaTeX{}", 'tex')
:call IMAP('LIG', "\<CR>\\vspace{\\baselineskip}\<CR>", 'tex')
:call IMAP('LIS', "\\begin{tuxliste}\<CR>\\item <++>\<CR>\\end{tuxliste}", 'tex')
:call IMAP('NOM', "\\Nombre{<+nombre+>}", 'tex')
:call IMAP('NOT', "\\Note{<+note+>}", 'tex')
:call IMAP('NDA', "\\NDAuteur{<+note+>}", 'tex')
:call IMAP('OUT', "\\Outil{<+Nom de l'outil+>}", 'tex')
:call IMAP('PAU', "\\begin{pause}{<+photo+>}{<+titre+>}\<CR><+texte+>\<CR>\\end{pause}", 'tex')
:call IMAP('PAC', "\\begin{pausecourte}\<CR><+texte+>\<CR>\\end{pausecourte}", 'tex')
:call IMAP('PHO', "\\maphoto{<+photo+>}", 'tex')
:call IMAP('PRO', "\\propre[<+index+>]{<+prenom+>}{<+nom+>}", 'tex')
:call IMAP('SEC', "\\Section{<+titre de la section+>}", 'tex')
:call IMAP('SIE', "\\siecle{<+siecle+>} siècle<++>", 'tex')
:call IMAP('TIV', "\\titrevelo{<+type r e p ou a+>}[<+option+>]{<+destination ou titre+>}", 'tex')
:call IMAP('TOU', "\\Touche{<+touche+>}", 'tex')
:call IMAP('UNI', "\\Unite{<+nombre+>}{<+unite+>}", 'tex')
:call IMAP('URL', "\\Urlref{<+url+>}{<+nom affiché+>}", 'tex')
:call IMAP('ZCOC', "\\lstinline$<+code en ligne+>$", 'tex')

" End of abbreviations ----------------------------------- }}}

" To use with external programs ---------------------------- {{{

" I want to get a pdf file at the output
:let g:Tex_DefaultTargetFormat='pdf'
" To use latexmk for the compiling process in case more than one pass is needed
":let g:Tex_CompileRule_pdf = 'latexmk -pdf -shell-escape $*'
:let g:Tex_CompileRule_pdf = '~/docker/latex/launchDocker.sh $*'
" I want the compiling process started with <F2>
:map <F2> <Leader>ll
" Use zathura to display pdf output
:let g:Tex_ViewRule_pdf = 'zathura'
" Launch zathura with <F3>
:map <F3> <Leader>lv

" End of the external programs links --------------------- }}}

" vim: ts=4 sw=4 noet
