#############################################################
#  Auteur : Stéphane CARPENTIER
#  Fichier : config.py
#    Modif : dim. 11 avril 2021 18:48
#
#############################################################

# Configuration files --------------------------------------- {{{

# I don't want autoconfig to be loaded
config.load_autoconfig(False)

# .Xressources ---------------------------------------------- {{{
# It looks likes it's working without that
# To use .Xressources for the colors
# import subprocess

# def read_xresources(prefix):
    # props = {}
    # x = subprocess.run(['xrdb', '-query'], stdout=subprocess.PIPE)
    # lines = x.stdout.decode().split('\n')
    # for line in filter(lambda l : l.startswith(prefix), lines):
        # prop, _, value = line.partition(':\t')
        # props[prop] = value
    # return props

# xresources = read_xresources('*')
# c.colors.statusbar.normal.bg = xresources['*.background']
# End of .Xressources --------------------------------------- }}}

# End of configuration files -------------------------------- }}}

# Content --------------------------------------------------- {{{

# Some defaults importants to me
c.content.default_encoding = "utf-8"
c.content.headers.accept_language = "fr,en-US,en"

# I hate autoplay video from youtube
c.content.autoplay = False

# For a little privacy
c.content.cookies.accept = "no-3rdparty"
c.content.geolocation = False

# End content ----------------------------------------------- }}}

# Visibility ------------------------------------------------ {{{

# Dark mode ------------------------------------------------- {{{
# I want a dark mode
c.colors.webpage.darkmode.algorithm = "lightness-cielab"
c.colors.webpage.darkmode.enabled = True

# But my images mustn't be modified
c.colors.webpage.darkmode.policy.images = "never"
# End Dark mode --------------------------------------------- }}}

# Hints ----------------------------------------------------- {{{
# I've got an azerty keyboard
c.hints.chars = "qsdfghjklm"

# In upper case to be easier to read
c.hints.uppercase = True
# End Hints ------------------------------------------------- }}}

# Zoom ------------------------------------------------------ {{{
# Default zoom
c.zoom.default = "125%"

# The default way is easier, but it's unlike my vim and zathura configs, so for consistency
config.bind('<Ctrl-=>', 'zoom')
config.bind('<Ctrl-->', 'zoom-out')
# The <Shift> is for azerty keyboards, probably not needed with qwerty keyboards
config.bind('<Shift-Ctrl-+>', 'zoom-in')
# End zoom -------------------------------------------------- }}}

# End visibility -------------------------------------------- }}}

# Various --------------------------------------------------- {{{
# I don't want to confirm before quitting, except if a download is in progress
c.confirm_quit = ["downloads"]

# To have time to read messages
c.messages.timeout = 10000
# End various ----------------------------------------------- }}}

# vim: ts=4 sw=4 noet
