#############################################################
#  Auteur : Stéphane CARPENTIER
#  Fichier : .bashrc
#    Modif : ven. 09 avril 2021 18:33
#
#############################################################

# The package lines ----------------------------------------- {{{

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# End of the package lines ---------------------------------- }}}

# Aliases --------------------------------------------------- {{{

# I prefer the colors
alias tree='tree -C'

# I'm not yet decided for the ls replacement to display the colors
#alias ls='exa'
#alias ll='exa -al'
#alias lc='exa -las=modified'
#alias ls='colorls'
#alias ll='colorls -Al'
#alias lc='colorls -Altr'
alias ls='lsd'
alias ll='lsd -Al'
alias lc='lsd -Altr'

# To get a pore complete ps when needed
alias psc='ps axwf -eo pid,user,cgroup,args'

# To get a better vision of my disk usage
alias du="dust"

# When I have a pdf in qutebrowser I have to download it to see
# The both lines are to be able to look at it and removing it without bothering with it's name
# It's probably better to have a shortcut for this in i3, I'll see that in another step
alias zd="ls -tr Downloads/ | tail -1 | xargs -I % zathura Downloads/%"
alias rd="ls -tr Downloads/ | tail -1 | xargs -I % rm Downloads/%"

# End of aliases --------------------------------------------- }}}

# Powerline settings ---------------------------------------- {{{

# Interesting information displayed in a colored way
powerline-daemon -q
POWERLINE_BASH_CONTINUATION=1
POWERLINE_BASH_SELECT=1

. /usr/share/powerline/bindings/bash/powerline.sh

# End of Powerline settings --------------------------------- }}}

# Information displayed at the terminal start --------------- {{{

# Once ready display information
neofetch

# Then for a little fun short jokes
cowthink -s $(fortune -as)

# End of displayed programs ---------------------------------- }}}

# Ugly things to clarify later ------------------------------- {{{
# Don't know why it doesn't work in i3/config a why it's not launched in .xinitrc
#xmodmap ~/.Xmodmap
# End of Ugly lines ------------------------------------------ }}}

# vim: ts=4 sw=4 noet
