#############################################################
#  Auteur : Stéphane CARPENTIER
#  Fichier : .bash_profile
#    Modif : ven. 09 avril 2021 22:41
#
#############################################################

# The package lines ----------------------------------------- {{{

[[ -f ~/.bashrc ]] && . ~/.bashrc

# End of the package lines ---------------------------------- }}}

# slrn personalisation -------------------------------------- {{{

# To be used by slrn
NNTPSERVER='news.free.fr' && export NNTPSERVER

# End of slrn personalisation ------------------------------- }}}

# Bash personalisation -------------------------------------- {{{

# I don't want lines twice in my .bash_history
HISTCONTROL=ignoredups && export HISTCONTROL

# To have a huge .bash_history
HISTSIZE=10000 && export HISTSIZE

# End of bash personalisation ------------------------------- }}}

# My preferred editors -------------------------------------- {{{

# To use gvim and not Emacs by default
export VISUAL="gvim -f"
export EDITOR="gvim -f"

# To get colors in man pages
export PAGER="most"

# End of editors' choice ------------------------------------- }}}

# the graphical session -------------------------------------- {{{

# To be launch the VM at login only on tty1
# See if it's the best later
if [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
  exec startx
fi

# End of graphical session ---------------------------------- }}}

# vim: ts=4 sw=4 noet
