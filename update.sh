#!/bin/bash
#############################################################
#  Auteur : Stéphane CARPENTIER
#  Fichier : update.sh
#    Modif : lun. 13 juil. 2020 21:40
#
#############################################################

# General information -------------------------- {{{

#############################################################
#
# The purpose of this script is to update the config files
# Either to update the Git repository with the new files name
# Or to update the user's config  files from the git repository
#
# The variables will be check in this order :
# 1- the default values in this script
# 2- the /etc/blamrc file for the admin to put default for every user
# 3- the ~/.config/blam/config
# 4- the command line
# to be sure an update won't change personalisation and a valid option
# is always provided.
#
#############################################################

# End of the general information --------------- }}}

# Variables definitions ------------------------ {{{

# Internal variables unmodifiable outside of the script ----------- {{{

# Options available for the script by the getopt function
# -h --help to display help other options not taken care of
# -c --commit to save the files in the git repo : incompatible with -u --update
# -u --update to update the files from the git repo : incompatible with -c --commit
# -p --print to do nothing but display what would have been done
short_opt='h,c,u,p'
long_otp='help,commit,update,print'

# End of the internal variables' definitions ---------------------- }}}

# Variables which can be changed by the end user ------------------ {{{

# By default, the commit is assumed because the help is only requested on demand
# and the update will be requested by the option given by the script. The available
# values for the order are COMMIT and UPDATE
order_copy='UPDATE'

# By default the script is assumed to run, not to print its output
print_output=0

# Repositories
git_repo='.'
user_repo='~'

# End of the variables updatable by the end user ------------------ }}}

# End of the variable definitions -------------- }}}

# vim: ts=4 sw=4 noet
